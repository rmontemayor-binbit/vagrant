#!/usr/bin/env bash

# Based on https://github.com/jackdb/pg-app-dev-vm/blob/master/Vagrant-setup/bootstrap.sh

# Edit whit your application's default
APP_DB_USER=battlecruiser
APP_DB_PASS=battlecruiser
APP_DB_NAME=devbinbit

###########################################################
# Changes below this line are probably not necessary
###########################################################
PG_VERSION=9.5
PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"

export DEBIAN_FRONTEND=noninteractive
apt-cache search postgresql
apt-get -y update
apt-get -y upgrade
apt-get -y install postgresql postgresql-contrib


# Edit postgresql.conf to change listen address to '*':
sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"
# Explicitly set default client_encoding
sed -i "s/#client_encoding = sql_ascii/client_encoding = utf8/" "$PG_CONF"
# Append to pg_hba.conf to add password auth:
echo "host    all             all             all                     md5" >> "$PG_HBA"

systemctl restart postgresql
systemctl enable postgresql


cat << EOF | su - postgres -c psql
-- Create the database user:
CREATE USER $APP_DB_USER WITH PASSWORD '$APP_DB_PASS' LOGIN;

-- Create the database:
CREATE DATABASE $APP_DB_NAME WITH OWNER=postgres
                                  LC_COLLATE='en_US.utf8'
                                  LC_CTYPE='en_US.utf8'
                                  ENCODING='UTF8'
                                  TEMPLATE=template0;
-- Connect to the database:
\connect $APP_DB_NAME;

-- Create the schema for the user
CREATE SCHEMA AUTHORIZATION $APP_DB_USER;

EOF

echo "Successfully created PostgreSQL dev virtual machine."
